//
//  ViewController.swift
//  Tracker
//
//  Created by CJ Good on 11/7/20.
//  Copyright © 2020 CJ Good. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UIActionSheetDelegate {
    
    // Array of objects from CoreData
    var events: [NSManagedObject] = []

    // Table View
    @IBOutlet weak var EventTableView: UITableView!
    
    // Handle the view loading
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Event Tracker"
        EventTableView.delegate = self
    }
    
    // Handle TableView Data as the view appears
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Event")
        do {
            events = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not load data: \(error)")
        }
    }
    
    // Add new event
    @IBAction func addEvent(_ sender: Any) {
        createEventAlert()
    }
    
    // Handle the UIAlertController for event creation
    func createEventAlert() {
        // Define the UIAlertController
        let alert = UIAlertController(
            title: "New Event",
            message: "Add a new event:",
            preferredStyle: .alert
        )

        // Title Text Field
        alert.addTextField(configurationHandler: {
            (title) in
            title.text = ""
            title.placeholder = "Title of event..."
        })
        
        // Detail Text Field
        alert.addTextField(configurationHandler: {
            (detail) in
            detail.text = ""
            detail.placeholder = "Symptoms of event..."
        })
        
        // Save Action --> Saves title and symptoms
        alert.addAction(UIAlertAction(title: "Save", style: .default) {
            [unowned self] action in
            let title = alert.textFields?[0].text
            let detail = alert.textFields?[1].text
            // Convert current datetime to a string
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US")
            dateFormatter.dateFormat = "h:mm a 'on' MMMM dd, yyyy"
            let now = dateFormatter.string(from: Date())
            let date = now
            let datetime = Date()
            
            if title == "" {
                print("Please enter a valid title")
            } else {
                self.save(title: title! as String, detail: detail! as String, date: date as String, datetime: datetime as Date)
                self.EventTableView.reloadData()
            }
        })
        // Cancel Action
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        // Present the UIAlertController
        present(alert, animated: true)
    }
    
    // Save the data to CoreData
    func save(title: String, detail: String, date: String, datetime: Date) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Event", in: managedContext)!
        let event = NSManagedObject(entity: entity, insertInto: managedContext)
        event.setValuesForKeys(
            [
                "title": title,
                "detail": detail,
                "date": date,
                "datetime": datetime
            ]
        )
        do {
            try managedContext.save()
            events.append(event)
        } catch let error as NSError {
            print("Error while saving: \(error)")
        }
    }

}
// Handle Table View Properties
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    // Set the number of table rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        events.count
    }
    
    // Create the custom cell views
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EventCell = EventTableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! EventCell
        let event = events[indexPath.row]
        cell.titleLabel?.text = event.value(forKeyPath: "title") as? String
        cell.detailLabel?.text = event.value(forKeyPath: "detail") as? String
        cell.dateLabel?.text = event.value(forKeyPath: "date") as? String
        return cell
    }
    
    // Enable deleting of rows
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // remove the deleted item from the model
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(events[indexPath.row] as NSManagedObject)
            events.remove(at: indexPath.row)
            EventTableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            createEventAlert()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.EventTableView.deselectRow(at: indexPath, animated: true)
        let eventDetails = UIAlertController(title: "Event Details", message: "Event Details", preferredStyle: .actionSheet)
        eventDetails.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) -> Void in
            print("Done Pressed")
        }))
        self.present(eventDetails, animated: true, completion: nil)
    }
    
}
