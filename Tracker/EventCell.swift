//
//  EventCell.swift
//  Tracker
//
//  Created by CJ Good on 11/10/20.
//  Copyright © 2020 CJ Good. All rights reserved.
//

import Foundation
import UIKit

class EventCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
}
